/*
NTP Wifi serial clock
	Created 5 june 2018 by Pierre Brial

Based on Udp NTP Client :
	created 4 Sep 2010 by Michael Margolis
	modified 9 Apr 2012  by Tom Igoe
	updated for the ESP8266 12 Apr 2015 
	by Ivan Grokhotkov
*/

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "SoftwareSerial.h"

#define TZ 4	// Time Zone

SoftwareSerial mySerial(D1,D2,true); // RX, TX, inverse logic

boolean b=0;
unsigned long t=0,t0;
char ssid[] = "MyWifiSSID";  //  your network SSID (name)
char pass[] = "MyWifiPassword";       // your network password

unsigned int localPort = 2390;      // local port to listen for UDP packets

IPAddress timeServerIP;
const char* ntpServerName = "pool.ntp.org";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address)
	{
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;
	udp.beginPacket(address, 123); //send a packet requesting a timestamp, port 123
	udp.write(packetBuffer, NTP_PACKET_SIZE);
	udp.endPacket();
	}

unsigned long getNTPtime()
	{
	unsigned long highWord,lowWord,secsSince1900;
	WiFi.hostByName(ntpServerName, timeServerIP); 
	sendNTPpacket(timeServerIP); // send an NTP packet to a time server
	// wait to see if a reply is available
	delay(500);
	int cb = udp.parsePacket();
	if (!cb)
		{
		return 0;
		}
	else
		{
		udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
		//the timestamp starts at byte 40 of the received packet and the integer
		// part is four bytes, or two words, long. First, extract the two words:
		highWord = word(packetBuffer[40], packetBuffer[41]);
		lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// and add 1 if fraction of seconds (byte 44) is greater than 0.5
		// This is NTP time (seconds since Jan 1 1900):
		secsSince1900 = (highWord << 16 | lowWord) + (packetBuffer[44]>128); 
		return secsSince1900;
		}
	}
		
void setup()
	{
	pinMode(LED_BUILTIN, OUTPUT);
	mySerial.begin(1200);
	WiFi.begin(ssid, pass);
	
	while (WiFi.status() != WL_CONNECTED)
		{
		delay(500);
		digitalWrite(LED_BUILTIN,b);
		b=1-b;
		}
	
	udp.begin(localPort);
	while(t==0) t=getNTPtime();
	t0=millis();
	}

void loop()
	{
	byte h=0,m=0,s=0;
	char heure[]="00:00:00";
	unsigned long t1=millis(),ntp;
	if ((t1-t0)>999)
		{
		t=t+(t1-t0)/1000;
		t0=t1;
		h=((t+TZ*3600) % 86400L) / 3600;
		m=(t  % 3600) / 60;
		s=t % 60;
		if ((m==30) && (s==30))
			{
			ntp=getNTPtime();
			if (ntp && t!=ntp) t=ntp;
			}
		heure[0]=h/10+48;
		heure[1]=h%10+48;
		heure[3]=m/10+48;
		heure[4]=m%10+48;
		heure[6]=s/10+48;
		heure[7]=s%10+48;
		mySerial.println(heure);
		analogWrite(LED_BUILTIN,1000+b*23);
		b=1-b;
		}
	}
	