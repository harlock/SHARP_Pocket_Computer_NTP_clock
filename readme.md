# SHARP POCKET COMPUTERS
# Serial communication with Arduino related boards

![](http://tethys.re/80Calcs/Calculatrices/schematics/PC1460-Wemos-NTP.png)

From 1980 and during nearly twenty years,
SHARP Corporation produced a line of pocket computers programmable in BASIC and machine language.
They were neatly engineered machines, and, by some aspects, far more efficient than modern i-things and Androïd devices :
zero bugs, instant boot up, no updates needed, and monthes, yes, monthes of battery life.

These pocket computers are built around SC61860 or SC62015 CMOS 8 bits CPU,
and some models have a Serial Input Output interfaces (SIO) :
[PC-1350](https://en.wikipedia.org/wiki/Sharp_PC-1350), PC-1360, PC-1460, PC-1475,
[PC-E500S](https://commons.wikimedia.org/wiki/File:PC-E500S.jpg),...

SHARP PC SIO has a 0-6V voltage, so it could communicate to an Arduino without level shifting.
Nevertheless, contrary to the Arduino, SHARP PC uses an inverse logic : 0V is HIGH and 6V is LOW.
To make themselves communicate, levels should be inverted, either by hardware or by software.

## With SoftwareSerial library
### Circuit
![](http://tethys.re/80Calcs/Calculatrices/schematics/Sharp-arduino.png)
This way you don't need any other component.

The pocket computer works with two CR 2032 lithium battery,
whose capacity is too weak for the Arduino.
So it is not possible to power the Arduino with SHARP Vcc pins, except if the SHARP is itself on external power source (5-6V).

### Code

The [SoftwareSerial example](https://www.arduino.cc/en/Tutorial/SoftwareSerialExample) can be used, with the following changes :

* set SoftwareSerial() third parameter to true, to activate inverse logic
* set begin() speed to a value compatible with your SHARP PC (usually 1200 bps)

Here is the code for the Arduino:

	#include <SoftwareSerial.h>
	
	SoftwareSerial mySerial(10,11,true); // RX, TX, inverse logic
	
	void setup()
		{
		// Open serial communications on standart port:
		Serial.begin(9600);
		// set the data rate for the SoftwareSerial port:
		mySerial.begin(1200);
		}
	
	void loop()
		{
		if (mySerial.available()) Serial.write(mySerial.read());
		if (Serial.available()) mySerial.write(Serial.read());
		}

Upload this sketch to the Arduino, and open the serial monitor.

On SHARP PC, type the following commands :

    OPEN "1200,N,8,1,A,F,&1A"
    LPRINT "HELLO ARDUINO"

The serial port parameters are kept in variable `OPEN$`, so next time, simply open the port with command `OPEN` alone.
    

## With hardware inverter

A logic NOT gate, like the [HEF4069](http://www.nxp.com/documents/data_sheet/HEF4069UB.pdf), could be inserted between the two devices, to inverse the signal.
You can then use the standard Arduino [serial functions](https://www.arduino.cc/reference/en/language/functions/communication/serial/).

![](http://tethys.re/80Calcs/Calculatrices/schematics/Sharp-arduino-inverter.png)

## Network capabilities

With Arduino ethernet or wifi shield, or with an ESP8266, you can now do fun tricks,
like connecting your SHARP PC to internet or wireless networks.

In the following circuit, an ESP8266 connects to Network Time Protocol servers and send time to the SHARP PC.
So the good old pocket computer become an highly accurate atomic clock :)

The ESP8266 uses 3.3V signals.
As the communication is from ESP to SHARP only, a simple NOT gate build with a mosfet and a resistor acts as inverter and level shifter :

![](http://tethys.re/80Calcs/Calculatrices/schematics/Sharp_NTP.png)

### Code

ESP8266 code : 
[NTPClient.ino](https://framagit.org/harlock/SHARP_Pocket_Computer_NTP_clock/blob/master/NTPClient.ino)

SHARP PC-1460 BASIC program:

	10:"A" CLEAR : CLOSE : OPEN "1200,N,8,1,A,F": WAIT 0
	15:DIM H$(0)*10
	20:INPUT #1H$(*)
	40:PRINT "        ";H$(0): CALL 1208: GOTO 20
	
`CALL 1208` is a system call specific to PC-1460. It forces LCD display to remain on while BASIC program is running.

The routine is at different addresses on other models :

| Model      |  CALL  |
| :----------| :----: |
|PC-1250/51  |  4576  |
| PC-1261/62 |  1083  |
| PC-1350    |  1201  |
| PC-1360    |  5064  |
| PC-1401/02 |  1442  |
| PC-1403/60 |  1208  |
| PC-2500    |  1326  |

For PC-1475, PC-E500S and later models, these system calls are no longer needed. Here is the code for PC-1475 :

	10:"A" CLEAR : CLOSE : OPEN "1200,N,8,1,A,F"
	15:CLS : WAIT 0
	20:DIM H$(0)*10
	25:INPUT #1H$(*)
	30:CURSOR 8: PRINT H$(0): GOTO 25

## References

* [80's Calculators](http://tethys.re/80Calcs/Calculatrices/Sharp.html)
* [Les Ordipoches SHARP](http://destroyedlolo.info/sharp/adresses.html)
